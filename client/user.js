const client = require("seneca")()
.use("seneca-amqp-transport")
.use("basic")
.use("entity")
.client({
    type: "amqp",
    pin: "role:user,cmd:*",
    url: process.env.BROKER_URL
    //url: "amqp://itKFic0X:GpgyM6s7yizu8bn77ZZFJabp0Mn0xTET@brown-kingcup-357.bigwig.lshift.net:10224/_8M1QgJ3n0Ju"
});

const create = (name) => {
    return new Promise(function(resolve, reject) {
        client.act("role:user,cmd:create", { name: name }, (err, res) => {
            resolve(res);
        });
    });
};

module.exports = {
    create
};