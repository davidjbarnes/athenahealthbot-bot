module.exports = function(apiCredentials) {
    var module = {};

    var client = require("seneca")()
        .use("seneca-amqp-transport")
        .use("basic")
        .use("entity")
        .client({
            type: "amqp",
            pin: "role:athena,cmd:*",
            url: process.env.BROKER_URL
        });

        module.auth = function() {
            return new Promise(function(resolve, reject) {
                client.act("role:athena,cmd:auth", { key:apiCredentials.key, secret:apiCredentials.secret }, (err, res) => {
                    if (err) { console.error(err); reject(err); }
                        
                    resolve(JSON.parse(res.result));
                });
            });
        };

        module.createPatient = function(access_token, patient) {
            return new Promise(function(resolve, reject) {
                client.act("role:athena,cmd:createPatient", {
                    access_token: access_token,
                    patient: patient
                }, (err, res) => {
                    if (err) { console.error(err); reject(err); }
                        
                    resolve(res.result);
                });
            });
        };

        module.getPatient = function(access_token, patientId) {
            return new Promise(function(resolve, reject) {
                client.act("role:athena,cmd:getPatient", {
                    access_token: access_token,
                    patientId: patientId
                }, (err, res) => {
                    if (err) { console.error(err); reject(err); }
                        
                    resolve(res.result);
                });
            });
        };

    return module;
};