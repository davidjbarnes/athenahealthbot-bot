const client = require("seneca")()
.use("seneca-amqp-transport")
.use("basic")
.use("entity")
.client({
    type: "amqp",
    pin: "role:conversation,cmd:*",
    url: process.env.BROKER_URL
});

const log = (text) => {
    return new Promise(function(resolve, reject) {
        client.act("role:conversation,cmd:log", { text: text }, (err, res) => {
            resolve(res);
        });
    });
};

module.exports = {
    log
};