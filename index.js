var restify = require("restify");
var builder = require("botbuilder");
var server = restify.createServer();

server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log("%s listening to %s", server.name, server.url); 
});

var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

server.post("/bot", connector.listen());

var userService = require("./client/user");
var conversationService = require("./client/conversation");
var athenaService = require("./client/athena")({ key:process.env.ATHENA_API_KEY, secret:process.env.ATHENA_API_SECRET });

var bot = new builder.UniversalBot(connector, function (session) {
    athenaService.auth().then((a)=>{

        session.send("Athena auth responded with access_token: %s", a.access_token);

        athenaService.createPatient(a.access_token, {
            practiceid:195900,
            departmentid:1,
            dob:"12/11/1979",
            zip:"84098",
            firstname:"Mikey",
            lastname:"Tyler"
        }).then((patientId)=>{
            athenaService.getPatient(a.access_token, patientId).then((patient)=>{
                session.send("Athena created patient: %s", JSON.stringify(patient));
            });
            
        }).catch((err)=>{
            session.send("An error occured: %s", err);
        });
    }).catch((err)=>{
        session.send("An error occured: %s", err);
    });
});